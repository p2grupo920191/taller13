
bin/prueba: obj/hilos.o obj/Ejemplo.o obj/slaballoc.o obj/rhaegal.o obj/drogon.o obj/viserion.o
	gcc obj/hilos.o obj/Ejemplo.o obj/slaballoc.o obj/rhaegal.o obj/drogon.o obj/viserion.o -o bin/prueba -lpthread	

obj/hilos.o: src/hilos.c
	gcc -Wall -w -c -Iinclude/ src/hilos.c -o obj/hilos.o

obj/Ejemplo.o: src/Ejemplo.c
	gcc -Wall -c -Iinclude/ src/Ejemplo.c -o obj/Ejemplo.o

obj/slaballoc.o: src/slaballoc.c
	gcc -Wall -c -Iinclude/ src/slaballoc.c -o obj/slaballoc.o

obj/rhaegal.o: src/rhaegal.c
	gcc -Wall -c -Iinclude/ src/rhaegal.c -o obj/rhaegal.o

obj/drogon.o: src/drogon.c
	gcc -Wall -c -Iinclude/ src/drogon.c -o obj/drogon.o

obj/viserion.o: src/viserion.c
	gcc -Wall -c -Iinclude/ src/viserion.c -o obj/viserion.o

.PHONY: clean
clean:
	rm bin/* obj/*.o

