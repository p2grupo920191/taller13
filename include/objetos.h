//En este archivo ud podra definir los objetos que podrán ser usados 
//con el slab allocator. Declare tambien el constructor y destructor de 
//cada objeto declarado. Abajo se provee un objeto de ejemplo.
//Declare al menos tres objetos distintos

//El constructor siempre recibe un puntero tipo void al tipo de objeto,
//y su tamaño (tal como se declaro el puntero a funcion).
//No olvide implementar las funciones de crear y destruir de cada objeto.

typedef struct ejemplo{
    int a;
    float b[100];
    char *msg;
    unsigned int refcount;
} Ejemplo;

//Constructor
void crear_Ejemplo(void *ref, size_t tamano);

//Destructor
void destruir_Ejemplo(void *ref, size_t tamano);

//TODO: Crear 3 objetos mas



//objeto 1/*
typedef struct Rhaegal{
    int i;
    float arr[100];
    char *msg;
    unsigned int count;
} Rhaegal;

//Constructor
void crear_Rhaegal(void *ref, size_t tamano);

//Destructor
void destruir_Rhaegal(void *ref, size_t tamano);





//objeto2
typedef struct Viserion{
    double a;
    int b[100];
    char *msg;
    int status;
} Viserion;

//Constructor
void crear_Viserion(void *ref, size_t tamano);

//Destructor
void destruir_Viserion(void *ref, size_t tamano);





//objeto3
typedef struct Drogon{
    unsigned long p[100];
    int l;
    char *msg;
    float status;
} Drogon;

//Constructor
void crear_Drogon(void *ref, size_t tamano);

//Destructor
void destruir_Drogon(void *ref, size_t tamano);


typedef struct objetoEjemplo{
    int id_hilo;
    int *data;
} objeto_ejemplo;

void crear_objeto_ejemplo(void *ref, size_t tamano);
void destruir_objeto_ejemplo(void *ref, size_t tamano);

