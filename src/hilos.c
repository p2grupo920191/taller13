#include "slaballoc.h"
#include <stdio.h>
#include "objetos.h"
#include <string.h>
#include <semaphore.h>
#include <stdlib.h>
#include <pthread.h>
#include <ctype.h>

typedef struct idcache
{
	SlabAlloc *cache;
	int id;
}IDCache;

void *funcion_hilo(void * a);

sem_t semaforo;

int main(int argc, char **argv) {
	char str[1];
	sem_init(&semaforo, 0,1);

	if (argc != 3) {
		fprintf(stderr, "usar: %s -n <# de hilos>\n", argv[0]);
		exit(0);
	}

	int hilos;
	hilos = atoi(argv[2]);

	char *nombre="hola";
	size_t tamano_objeto=sizeof(objeto_ejemplo);	
	SlabAlloc *cache=crear_cache(nombre, tamano_objeto,crear_objeto_ejemplo,destruir_objeto_ejemplo);
	printf("---------------------------------------------------MOSTRAR CACHE---------------------------------------------");
	stats_cache(cache);
	pthread_t threads[hilos];
	printf("Ingresar por teclado 'V' (mayuscula) para validar\n");
	scanf("%s",str);
	toupper(str[0]);
	if((strcmp(str,"V"))!=0){return -1;}
	
	
	for(int i = 0; i < hilos; i++){

		IDCache *idcache= malloc(sizeof(IDCache));
		idcache-> id = i;
		idcache-> cache = cache;

		if(pthread_create(&threads[i], NULL, funcion_hilo, idcache)){
        	printf("\n ERROR CREANDO HILO 1");
        	exit(1);
    	}
	printf("---------------------------------------------------VALIDAR CACHE----------------------------------------------");
	stats_cache(cache);
	}

	for(int i = 0; i < hilos; i++){

		pthread_join(threads[i], NULL);
	}
	return 0;
}

void * funcion_hilo(void * a){

	IDCache *idcache = (IDCache *)a;
	
	void *objetos[1000];
	
	for(int i = 0; i < 1000; i++){
		
		sem_wait(&semaforo);
		void * obj = obtener_cache(idcache->cache, 1);
		sem_post(&semaforo);

		objeto_ejemplo *ob = (objeto_ejemplo *)obj;
		ob -> id_hilo = idcache->id;
		ob->data[0] = 0 * idcache->id;
		ob->data[1] = 1 * idcache->id;
		ob->data[2] = 2 * idcache->id;

		objetos[i] = obj;

	}


	for(int i = 700; i < 1000; i++){
		sem_wait(&semaforo);
		devolver_cache(idcache->cache, objetos[i]);
		sem_post(&semaforo);
	}

	
	for(int i = 0; i < 700; i++){
		objeto_ejemplo * ob = objetos[i];
		if(ob->id_hilo != idcache->id || ob->data[0] != 0 * idcache->id || ob->data[1] != 1 * idcache->id || ob->data[2] != 2 * idcache->id ){
			printf("id_hilo esperados: \n hilo %d, id[0]: %d, id[1]: %d, id[2]: %d\n", idcache->id, idcache->id * 0, idcache->id * 1, idcache->id * 2);
			printf("id_hilo obtenidos\n: hilo%d, id[0]: %d, id[1]: %d, id[2]: %d", ob->id_hilo, ob->data[0], ob->data[1], ob->data[2]);
		}else{printf("los elementos tienen el mismo id esperado");
	}

	free(idcache);
}
