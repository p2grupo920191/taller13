#include "slaballoc.h"
#include <inttypes.h>

SlabAlloc *crear_cache(char *nombre, size_t tamano_objeto, constructor_fn constructor, destructor_fn destructor){
	//Reservamos memoria para el Slaballoc
	SlabAlloc *alloc=malloc(tamano_objeto*TAMANO_CACHE);	//Region memoria de SlabAlloc
	void *mem=malloc(tamano_objeto*TAMANO_CACHE);	//Region memoria objetos
	Slab *arr_sl=malloc(sizeof(Slab)*TAMANO_CACHE);	//Region memoria Slabs
	//ASIGNACION DE DATOS AL ALLOCATOR
	alloc->nombre=nombre;
	alloc->mem_ptr = mem;
	alloc->tamano_cache = TAMANO_CACHE;
	alloc->cantidad_en_uso = 0;
	alloc->tamano_objeto = tamano_objeto;
	alloc->constructor = constructor;
	alloc->destructor = destructor;
	alloc->slab_ptr=arr_sl;
	   
	for(int i=0;i<TAMANO_CACHE;i++){
	     (arr_sl+i)->ptr_data = mem+(i*tamano_objeto);	//Inicializamos los Slabs
	     (arr_sl+i)->status = DISPONIBLE;				//Inicializamos el status de cada Slab
	     constructor((mem+(i*tamano_objeto)),tamano_objeto);		//Inicializamos los Objetos
	   }
	return alloc;
	}


void *obtener_cache(SlabAlloc *alloc, int crecer){
   Slab *sl=alloc->slab_ptr;
   void *ptdat=NULL;
   int i;
   if(crecer==1 && ((alloc->cantidad_en_uso) == (alloc->tamano_cache))){
	//Copia de PTR_DATA a PTR_OLD previo a realloc
	for (i=0;i< alloc->tamano_cache;i++){
		(sl+i)->ptr_data_old = (sl+i)->ptr_data;
		}
	//REALLOC
	sl = realloc(alloc->slab_ptr,2*(alloc->tamano_cache)*(sizeof(Slab)));
	alloc->mem_ptr = realloc(alloc->mem_ptr,2*(alloc->tamano_cache)*(alloc->tamano_objeto));
	
	for(i=0; i< alloc->tamano_cache; i++){	//For para asegurarnos que los primeros Slabs estan en uso
		(sl+i)->status = EN_USO;
	}
	for(i=0; i< alloc->tamano_cache; i++){
	   (sl+i)->ptr_data = ((alloc->mem_ptr)+(i*(alloc->tamano_objeto)));
	 
	}
	alloc->tamano_cache = 2*(alloc->tamano_cache);

   }
   	//En el caso de tener disponibilidad en cache
   if ((alloc->cantidad_en_uso) < (alloc->tamano_cache)){
	   for(i=0;i < alloc->tamano_cache;i++){
		if((sl+(i))->status == 0){
			ptdat = (sl+(i))->ptr_data;
			alloc->cantidad_en_uso++;
			(sl+(i))->status = EN_USO;
			//break;
			return ptdat;
		}
	    }
   }
   return ptdat;
}




void devolver_cache(SlabAlloc *alloc, void *obj){
	Slab *arr_Slabs=alloc->slab_ptr;
	int i;
	for(i=0;i<(alloc->tamano_cache);i++){
		if(arr_Slabs[i].ptr_data==obj || (arr_Slabs[i].ptr_data_old==obj && arr_Slabs[i].ptr_data_old!=NULL)){
			arr_Slabs[i].status=DISPONIBLE;
			alloc->cantidad_en_uso=alloc->cantidad_en_uso-1;
			
		}
	}	
}

void destruir_cache(SlabAlloc *cache){
 // Slab *arr_sl=cache->slab_ptr;
  int i;
  for (i=0;i< cache->tamano_cache;i++){
     void *obj=(cache->mem_ptr)+(i*cache->tamano_objeto);
     cache->destructor(obj,cache->tamano_objeto);
  }
  for (i=0; i< cache->tamano_cache; i++){
	Slab *ptr = (cache->slab_ptr)+(i*sizeof(Slab));
	(ptr->ptr_data )= NULL;
	(ptr->ptr_data_old) = NULL;
	(ptr->status )= 0;
  }
 cache->nombre=NULL;
 cache->tamano_cache=0;
 cache->cantidad_en_uso=0;
 cache->tamano_objeto=0;
 cache->constructor=NULL;
 cache->destructor=NULL;
  free(cache->slab_ptr);
  free(cache->mem_ptr);
 cache->mem_ptr=NULL;
 cache->slab_ptr=NULL;
  free(cache);

}

void stats_cache(SlabAlloc *cache){
printf("Nombre de cache:\t%s\n",cache->nombre);
printf("Cantidad de slabs:\t%d\n",cache->tamano_cache);
printf("Cantidad de slabs en uso:\t%d\n",cache->cantidad_en_uso);
printf("Tamano de objeto:\t%ld\n",cache->tamano_objeto);
printf("Direccion de cache->mem_ptr:\t%p\n",cache->mem_ptr);
Slab *sl =cache->slab_ptr;
for(int i=0;i<cache->tamano_cache;i++){
   if(((sl+(i))->status)==0){
      printf("Direccion ptr[%d].ptr_data:\t%p\tDISPONIBLE\t  ptr_data_old\t%p\n",i,((sl->ptr_data)+(i*(cache->tamano_objeto))),((sl->ptr_data)+(i*(cache->tamano_objeto))));
   }else{
      printf("Direccion ptr[%d].ptr_data:\t%p\tEN_USO\t  ptr_data_old\t%p\n",i,((sl->ptr_data)+(i*(cache->tamano_objeto))),((sl->ptr_data)+(i*(cache->tamano_objeto))));}

}

}
